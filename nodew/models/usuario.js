var mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
var Reserva = require('./reserva');
const bcrypt = require('bcrypt ');
const crypto =  require('crypto');
const { defaultMaxListeners } = require('stream');
const saltRounds =  10;
var Schema = mongoose.Schema;
const  validateEmail = function(email){
    const re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return re.test(email);
};
var usuarioSchema = new Schema({
    nombre:{
        type:  String,
        trim:  true,
        required: [true, 'el mensaje es obligatorio']
    },
    email:{
        type:  String,
        trim:  true,
        required: [true, 'el email es obligatorio'],
        lowercase: true,
        unique: true,
        validate: [validateEmail,'porfavor , ingrese un Email valido'],
        match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/]    
    },
    password:{
        type: String,
        required: [true, 'el Pasword es obligatorio']
    },
    passwordResetToken: String,
    passwordResetTokenExpires: Date,
    verificado:{
        type: Boolean,
        default:false
    }
    
});
usuarioSchema.plugin(uniqueValidator, {message: ' El {PATH} ya existe con otro usuario.'});
usuarioSchema.pre('save', function(next){
    if(this.isModified('password')){
        this.password = bcrypt.hashSync(this.password, saltRounds);
    }
    next();

});
usuarioSchema.methods.validPassword = function(password){
    return bcrypt.compareSync(password, this.password);
}
usuarioSchema.methods.reservar = function(biciId, desde, hasta, cb){
    var reserva = new Reserva({usuario: this._id, bicicleta: biciId, desde: desde, hasta: hasta});
    console.log(reserva);
    reserva.save(cb);
}
usuarioSchema.methods.enviar_email.bienvenida = function(cb){
    const token = new Token({_userId: this.id, token: crypto.randomBytes(16).toString('hex')});
    const email_destination = this.email;
    token.save(function(err){
        if(err){ return console.log(err.message);}
        const mailOptions = {
            from: 'no-reply@redbicicletas.com',
            to: email_destination,
            subject: 'verificacion de cuenta',
            text: 'hola,\n\n' + 'por favor, para verificar su cuenta haga click en este link: \n' + 'http://localhost:5000'  + '\/token/confirmation\/' + token.token + '.\n'
        };
        mailer.sendMail(mailOptions, function(err){
            if(err){ return console.log(err.message); }
            console.log('A verification email has been sent to ' + email_destination + '.');
        });
    });
}
module.exports = mongoose.model('usuario', usuarioSchema);