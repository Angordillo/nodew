var Bicicleta = require('../../models/bicicleta');
exports.bicicleta_list = function(req, res){
    Bicicleta.find({},function(err, bicicletas){
    	res.status(200).json({
        	bicicletas : Bicicleta.allBicis

    	});
    });
}
exports.bicicleta_create = function(req, res){
    var bici = new Bicicleta({req.body.id, req.body.color, req.body.modelo});
    bici.ubicacion = [req.body.lat, req.body.lng];
    Bicicleta.add(bici);
    res.status(200).json({
        bicicleta: bici

    });
}

exports.bicicleta_update = function(req, res){
    
    Bicicleta.findById(req.body.id, function(error, bici){
        
        
        bici.color = req.body.color;
        bici.modelo  = req.body.modelo;
        bici.ubicacion = [req.body.lat, req.body.lng];
        
        Bicicleta.update(bici, (err, raw) => {

            res.status(200).json({
                bicicleta: bici
            });

        });
    }); 
}
exports.bicicleta_delete = function(req, res){
    Bicicleta.removeById(req.body.id);
    res.status(204).send();
}